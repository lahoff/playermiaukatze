public class Battle {

  private static Player newPlayerA(int[] multiset, int target) {
    return new Player1(multiset, target);
  }

  private static Player newPlayerB(int[] multiset, int target) {
    return new Player2(multiset, target);
  }

  public static void main(String[] args) {
    int[] set = {2,3,4,7,8,9,10,11};
    int n = 4;
    int target = 22;
    /*
    int[] set = {1,2,3,4,5};
    int n = 8;
    int target = 20;
    */
    /*
    int[] set = {1,2,3,4,17,18,19,20};
    int n = 5;
    int target = 40;
    */

    int matches = 100000;


    int[] multiset = multisetFromSet(set,n);
    int aWins = 0;
    int bWins = 0;

    int[] shuffleset = multiset.clone();


    for(int i = 0; i<matches; i++) {
      Player aPlayer = newPlayerA(multiset, target);
      Player bPlayer = newPlayerB(multiset, target);

      WinState winner = match(shuffleset,target,aPlayer,bPlayer);
      if(winner == WinState.AWins) aWins++;
      if(winner == WinState.BWins) bWins++;
    }

    // now with reversed roles
    for(int i = 0; i<matches; i++) {
      Player aPlayer = newPlayerA(multiset, target);
      Player bPlayer = newPlayerB(multiset, target);

      WinState winner = match(shuffleset,target,bPlayer,aPlayer);
      if(winner == WinState.AWins) bWins++;
      if(winner == WinState.BWins) aWins++;
    }

    System.out.println("Awinne: "+aWins);
    System.out.println("Bewinne: "+bWins);
  }

  static WinState match(int[] shuffleset, int target, Player aPlayer, Player bPlayer) {
    Shuffler shuffler = new Shuffler(shuffleset);
    int aSum = 0;
    int bSum = 0;
    boolean aWants = true;
    boolean bWants = true;
    while(aWants || bWants) {
      if(aWants) {
        int taken = shuffler.take();
        aSum += taken;
        if(aSum > target) return WinState.BWins;
        aWants = aPlayer.oneMore(taken);
      }

      if(bWants) {
        int taken = shuffler.take();
        bSum += taken;
        if(bSum > target) return WinState.AWins;
        bWants = bPlayer.oneMore(taken);
      }
    }
    if(aSum > bSum) return WinState.AWins;
    if(aSum < bSum) return WinState.BWins;
    return WinState.Stalemate;
  }



  static int[] multisetFromSet(int[] set, int n) {
    int[] multiset = new int[set.length*n];
    int index = 0;
    for(int i = 0; i<n; i++) {
      for(int value:set) {
        multiset[index] = value;
        index++;
      }
    }
    return multiset;
  }

}

enum WinState {
  AWins,
  BWins,
  Stalemate
}
