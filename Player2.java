
public class Player2 extends Player {

	public Player2(int[] multiset, int target) {
		super(multiset, target);
	}

	public double expectedValue() {
		//for any array multiset & any target
		
		int allCardsSum = 0; 
		
		for (int i = 0; i < multiset.length; i++) {
			allCardsSum = allCardsSum + multiset[i];
		}
		
		return allCardsSum/multiset.length;
	}
	
	double expectedValue = this.expectedValue();
	
	int sumHandcards = 0;
	
	int amountHandcards = 0;
	
	public boolean oneMore(int value) {
		
		sumHandcards = sumHandcards + value;
		
		amountHandcards++;
		
		/* if (sumHandcards > target) {
			System.out.println("I lost this game ;-;");
			return false;
		}
		*/

		expectedValue = expectedValue - (Double.valueOf(value)/Double.valueOf(multiset.length));
		
		if ((target - sumHandcards) >= Math.round(expectedValue)) return true;

		return false;
	}
	
	public static void main(String[] hillo) {
		int[] cards = {1, 2, 3, 4, 5}; 
		int target = 7; 
		
		Player2 sina = new Player2(cards, target);
		
		sina.oneMore(2);
		sina.oneMore(3);
		sina.oneMore(4);
		
	}

}
