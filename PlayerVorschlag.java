class ateam extends Player {
  int hand;
  float risiko;
  float estimated_enemy_hand;

  public ateam(int[] multiset, int value) {
    super(multiset, value);
    this.hand = 0;
    this.risiko = 0;
    this.estimated_enemy_hand = expectation(multiset)*expected_pulls_enemy(multiset);

  }
  /*Zunächst braucht man folgende Dinge: Geschätzte Handzahl des Gegners, Erwartungswert des eigenen Zuges, eigene
   *Handzahl. Ist die eigene Handzahl so groß, dass sie die erwartete Handzahl des Gegners übertrifft, kann man false
   *zurückgeben. Ist sie das nicht, kann man zwei Fälle unterscheiden: aktuelle Handzahl + Erwartungswert des eigenen
   *Zuges <= target und Fall 2 "..." > target. Im ersteren Fall kann man natürlich true zurückgeben.
   *Im zweiteren Fall würde ich gerne eine Abwägung vornehmen. Auf der einen Seite steht die anhand der erwarteten
   *gegnerischen Hand geschätzte Wahrscheinlichkeit, dass der Handwert des Gegners oberhalb des eigenen aktuellen Handwertes
   *liegt. Auf der anderen Seite die anhand des Erwartungswertes des nächten Zuges ermittelte Wahrscheinlichkeit, dass
   *aktueller Handwert + nächster Zug > target. Dann soll in Abwägung dieser Wahrscheinlichkeiten true oder false zurückgegeben
   *werden.
   *
   */
  public boolean oneMore(int value) {
    hand = hand + value;
    boolean newcard;
    /* 1. Fall: hand ist >= target - min(multiset)
     * 2. Fall: hand ist < target - min(multiset)
     * Im zweiten Fall ist zu wissen, wie viele Karten die anderen Spieler
     * bereits gezogen haben. Daraus lässt sich dann folgern, wie viele
     * Karten noch zu ziehen sind und
     */
    boolean decision = ((hand + expectation(multiset) <= target)||(risk_fitting_cards(estimated_enemy_hand-hand,target-hand, multiset) >= risiko)) & ((hand <= expectation(multiset)));
    if(decision) {
      newcard = true;
    } else { newcard = false;

    }
    return newcard;
  }

  float risk_fitting_cards(float u,int l, int[] list) {
    int counter = 0;
    int g = list.length;
    for(int i=0; i<g;i++) {
      if ((list[i] > u) & (list[i] < l) ) {counter++;}
    }
    float e = counter/(list.length);
    return e;
  }
  float expectation(int[] list) {
    int l = list.length;
    float s = 0;
    float e;
    for(int i=0; i<list.length; i++) {
      s = s+list[i];
    }
    e = s/l;
    return e;
  }
  int expected_pulls_enemy(int[] list) {
    int i = 0;
    float e = expectation(list);
    while(i*e <= target) {
      i++;
    }
    return i;
  }
  float squared_expectation(int[] list) {
    int l = list.length;
    float s = 0;
    float e;
    for(int i=0; i<list.length; i++) {
      s = s+(list[i]*list[i]);
    }
    e = s/l;
    return e;
  }
  float sd(int[] list) {
    return (float) Math.sqrt(squared_expectation(list) - (expectation(list)*expectation(list)));
  }
  public int min(int[] list) {
    int minimum = list[0];
    int l = list.length;
    for(int i=1; i<l; i++) {
      if(list[i] < minimum) {minimum = list[i];}
    }
    return minimum;
  }
}
