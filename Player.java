
public abstract class Player
{
	// multiset of possible values
	public int[] multiset;
	
	// target sum which must not be exceeded
	public int target;
	
	public Player(int[] multiset, int target)
	{
		this.multiset = multiset;
		this.target = target;
	}
	
	/*
	 * This method gets called on every gameround. The 'value'-parameter contains the number
	 * which has been drawn for the player.
	 * 
	 * It should return 'true' if and only if the player wants to play another round.
	 */
	public abstract boolean oneMore(int value);
}
