
public class Player1 extends Player {

	public Player1(int[] multiset, int target) {
		super(multiset, target);
	}

	public double expectedValue() {
		//for any array multiset & any target
		
		int allCardsSum = 0; 
		
		for (int i = 0; i < multiset.length; i++) {
			allCardsSum = allCardsSum + multiset[i];
		}
		
		return allCardsSum/multiset.length;
	}
	
	double expectedValue = this.expectedValue();
	
	int handcards = 0;
	
	public boolean oneMore(int value) {
		
		handcards = handcards + value;
		
		/* if (handcards > target) {
			System.out.println("I lost this game ;-;");
			return false;
		}
		*/
		
		if ((target - handcards) >= Math.round(expectedValue)) return true;

		return false;
	}
	
	public static void main(String[] hillo) {
		int[] cards = {1, 2, 3, 4, 5}; 
		int target = 7; 
		
		Player1 torben = new Player1(cards, target);
		
		torben.oneMore(2);
		torben.oneMore(3);
		torben.oneMore(4);
		
	}
	
}
