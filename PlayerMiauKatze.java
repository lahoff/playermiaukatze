
public class PlayerMiauKatze extends Player {

	public PlayerMiauKatze(int[] multiset, int target) {
		super(multiset, target);
	}

	public double expectedValue() {
		//for any array multiset & any target
		
		int allCardsSum = 0; 
		
		for (int i = 0; i < multiset.length; i++) {
			allCardsSum = allCardsSum + multiset[i];
		}
		
		return allCardsSum/multiset.length;
	}
	
	double expectedValue = this.expectedValue();
	
	int sumHandcards = 0;
	
	int amountHandcards = 0;
	
	public boolean oneMore(int value) {
		
		sumHandcards = sumHandcards + value;
		
		amountHandcards++;
		
		/* if (sumHandcards > target) {
			System.out.println("I lost this game ;-;");
			return false;
		}
		*/

		expectedValue = expectedValue - (Double.valueOf(value)/Double.valueOf(multiset.length));
		
		if ((target - sumHandcards) >= Math.round(expectedValue*0.75)) return true;

		return false;
	}
}
