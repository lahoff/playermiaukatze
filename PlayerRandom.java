import java.util.Random;

public class PlayerRandom extends Player {
  public Random random;
	
	public PlayerRandom(int[] multiset, int target) {
    super(multiset, target);
    this.random = new Random();
	}
	
	public boolean oneMore(int value) {
    return random.nextBoolean();
  }
}
