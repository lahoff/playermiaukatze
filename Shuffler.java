import java.util.Random;
public class Shuffler {
  Random random;

  int[] shuffleset;
  int numberOfTaken = 0;

  public Shuffler(int[] shuffleset) {
    this.shuffleset = shuffleset;
    this.numberOfTaken = 0;
    this.random = new Random();
  }

  public int take() {
    // the cards already taken are swapped to the front
    // pick card not yet taken
    int takenIndex = random(numberOfTaken, shuffleset.length);
    int takenValue = shuffleset[takenIndex];

    // swap the taken number to front
    shuffleset[takenIndex] = shuffleset[numberOfTaken];
    shuffleset[numberOfTaken] = takenValue;

    numberOfTaken++;

    return takenValue;
  }

  int random(int lower, int upper) {
    return lower + random.nextInt(upper-lower);
  }

  public static void main(String[] marx) {
    int[] set = {1,1,2,3,6};
    Shuffler shuffler = new Shuffler(set);
    for(int i = 0; i<5; i++) 
      System.out.println(shuffler.take());

  }

}
